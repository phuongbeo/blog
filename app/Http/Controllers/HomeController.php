<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $name = "Phuong";
        $age = 20;
        return view('home', compact('name', 'age'));
    }

    public function account()
    {
        $name = "Phuong";
        $age = 20;
        return view('account', compact('name', 'age'));
    }
}
