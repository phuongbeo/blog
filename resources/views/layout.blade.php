<html>

<head>
    <title>app title @yield('title')</title>
    <style>
        body {
            width: 900px;
            margin: 0 auto;
        }

        .sidebar {
            width: 400px;
            float: left;
            background: #37474F;
            color: lightblue;
            text-align: center;
        }

        .main {
            width: 500px;
            background-color: aqua;
            color: red;
            height: 153px;
            float: right;
            text-align: center;

        }

        .footer {
            float: left;
            width: 100%;
            height: 60px;
            line-height: 25px;
            background: #aaa;
        }

        .footer>h1 {
            text-align: center;

        }
    </style>
</head>

<body>
    <div class="sidebar">
        @section('sidebar')
        @show
    </div>
    <div class="main">
        @yield('main')
    </div>
    <div class="footer">
        @section('footer')
        @show
    </div>
</body>

</html>