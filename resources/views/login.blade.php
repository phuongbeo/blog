<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        li{
            list-style: none;
            color: red;
        }
    </style>
</head>

<body>
    <h1>Login</h1>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
    <form action="/login" method="post">
        @csrf
        <input type="text" name="username" placeholder="username"><br>
        <input type="password" name="password" placeholder="password"><br>
        <input type="number" name="age" placeholder="age"><br>
        <button type="submit">Login</button><br>
    </form>
</body>

</html>