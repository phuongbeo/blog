@extends('layouts.app')

@push('styles')
    <style>
        
    </style>
@endpush

@section('content')
    <form action="/news/{{ $news->id }}" method="POST">
        @csrf
        @method('PUT')
        <label for="">Title</label> <br>
        <input type="text" name="title" value="{{ $news->title }}" required> <br>
        <label for="">Content</label> <br>
        <textarea name="content" id="" cols="30" rows="10" required>{{ $news->content }}</textarea> <br>
        <button>Update</button>
    </form>
@endsection

@push('scripts')
    
@endpush
